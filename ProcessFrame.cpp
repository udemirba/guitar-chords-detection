#include "projectHeader.h" // Include the header file

using namespace cv;
using namespace std;

Chord processFrame(Mat& frame_orig) {

            // Declare the variables
        Mat src_unblur, src, dst, color_dst;
        Mat frame_blur, frame_edge, frame_rotated;
        vector<Vec4i> lines, string_lines, fret_lines, lineSegments, segments, lines_pre;
        vector<double> slope, string_slope, fret_slope;
        double line_slope, hslope_diff, vslope_diff;
        double distanceToCheck = 0;

        double hthreshold = 0.1;
        double vthreshold = 0.3;

        GaussianBlur(frame_orig, frame_blur, Size(3, 3), 0, 0); // Apply Gaussian blur to the image to get rid of the noise

        Canny(frame_blur, frame_edge, 50, 200, 3); // Apply Canny Edge Detection to get the edges

        //showImage("Canny Edge Detection - Original Image", frame_edge);   

        // Apply the probabilistic Hough transform to find lines
        HoughLinesP(frame_edge, lines, 1, CV_PI / 180, 80, 200, 20); // 80,100,20  //  80,80,20

        // Calculate the main line's slope, since HoughLinesP sorts the lines we can take the first one in the lines vector
        double main_line_slope = (double(lines[1][1]) - double(lines[1][3])) / (double(lines[1][0]) - double(lines[1][2]));
        double main_vertical_slope = -1 / main_line_slope; // Calculate the slope that is perpendicular to the main slope, which corresponds to frets

        // Select the frets and strings in the lines vector and store them in two different vectors
        for (size_t i = 0; i < lines.size(); i++) {
            line_slope = (double(lines[i][1]) - double(lines[i][3])) / (double(lines[i][0]) - double(lines[i][2])); // particular line slope

            hslope_diff = abs(main_line_slope - line_slope); // calculating the difference wrt the main values to apply threshold
            vslope_diff = abs(main_vertical_slope - line_slope);

            if (hslope_diff < hthreshold) { // Strings
                string_lines.push_back(lines[i]);

                // Draw lines in the image
                //line(frame_orig, Point(lines[i][0], lines[i][1]), Point(lines[i][2], lines[i][3]), Scalar(0, 0, 255), 3, 8);            
            }
            else if (vslope_diff < vthreshold) { // Frets
                fret_lines.push_back(lines[i]);

                // Draw lines in the image
                //line(frame_orig, Point(lines[i][0], lines[i][1]), Point(lines[i][2], lines[i][3]), Scalar(0, 255, 0), 3, 8);
            }
        }

        // Find the largest cluster of lines
        double eps = 200.0; //350.0;  // Maximum distance between neighboring lines
        //int minPoints = 25;     // Minimum number of points required to form a cluster
        vector<Vec4i> largestCluster = findLargestCluster(string_lines, eps, frame_orig);

        Mat croppedImage = extractRoI(largestCluster, frame_orig);

        // Display the cropped ROI image
        //showImage("Cropped ROI Image", croppedImage);       

        double angle = atan(main_line_slope) * 180 / CV_PI; // Determining the angle for rotation

        // Get the center coordinates of the image to create the 2D rotation matrix
        Point2f center((double(frame_orig.cols) - 1) / 2.0, (double(frame_orig.rows) - 1) / 2.0);

        // Using getRotationMatrix2D() to get the rotation matrix
        Mat rotation_matrix = getRotationMatrix2D(center, angle, 1.0);

        // Rotate the image using warpAffine
        warpAffine(frame_orig, frame_rotated, rotation_matrix, frame_orig.size());

        Mat Fretboard = extractRotatedRoI(largestCluster, frame_rotated, rotation_matrix);

        // Display the cropped ROI image
        //showImage("Cropped Rotated ROI Image", Fretboard);

        Mat Fretboard2;
        Fretboard.copyTo(Fretboard2);

        Mat grayFretboard, edgesFretboard;
        cvtColor(Fretboard, grayFretboard, COLOR_BGR2GRAY);
        //GaussianBlur(grayFretboard, grayFretboard, Size(3, 3), 0);
        Canny(grayFretboard, edgesFretboard, 50, 150, 3); //50,200,3   

        Mat kernel = getStructuringElement(MORPH_RECT, Size(3, 3));
        dilate(edgesFretboard, edgesFretboard, kernel);
        //erode(edgesFretboard, edgesFretboard, kernel);

        //showImage("Detected Edges", edgesFretboard);

        cvtColor(Fretboard2, Fretboard2, COLOR_BGR2GRAY);
        GaussianBlur(Fretboard2, Fretboard2, Size(5, 5), 0, 0);


        Mat gradient, gradientAbs, thresholded, medianFiltered;
        // Scharr operator to find vertical lines
        Sobel(Fretboard2, gradient, CV_16S, 1, 0, -1);

        // Convert gradient image to absolute values
        convertScaleAbs(gradient, gradientAbs);

        // Apply thresholding
        threshold(gradientAbs, thresholded, 254, 255, THRESH_BINARY);

        // Apply median filter to thresholded image
        medianBlur(thresholded, thresholded, 3);

        // Perform morphological closing operation
        Mat morphKernel = getStructuringElement(MORPH_RECT, Size(7, 7));
        morphologyEx(thresholded, thresholded, MORPH_CLOSE, morphKernel);

        //showImage("Sobel Filter - Binary", thresholded);

        vector<Vec4i> linesFretboard, fretsFretboard;

        HoughLinesP(thresholded, linesFretboard, 1, CV_PI / 180, 20, 80, 10); //10 100 10
        for (size_t i = 0; i < linesFretboard.size(); i++) {

            if (linesFretboard[i][1] < linesFretboard[i][3]) {
                linesFretboard[i] = linesFretboard[i];
                fretsFretboard.push_back(linesFretboard[i]);

            }
            else {
                double temp1 = linesFretboard[i][0];
                double temp2 = linesFretboard[i][1];
                linesFretboard[i][0] = linesFretboard[i][2];
                linesFretboard[i][1] = linesFretboard[i][3];
                linesFretboard[i][2] = temp1;
                linesFretboard[i][3] = temp2;
                fretsFretboard.push_back(linesFretboard[i]);
            }
            //line(Fretboard, Point(linesFretboard[i][0], linesFretboard[i][1]),
            //   Point(linesFretboard[i][2], linesFretboard[i][3]), Scalar(0, 255, 255), 1, 8);
        }

        //showImage("Sobel Filter - Frets", Fretboard);

        // Merge similar lines
        vector<Vec4i> mergedLines;
        double maxDistanceDiff = 30;  // Maximum distance difference to consider lines similar
        for (size_t i = 0; i < fretsFretboard.size(); i++) {
            Vec4i checkingline = fretsFretboard[i];
            bool lineMerged = false;

            for (size_t j = 0; j < mergedLines.size(); j++)
            {
                Vec4i mergedLine = mergedLines[j];
                if (areLinesSimilar(checkingline, mergedLine, maxDistanceDiff))
                {
                    // Merge similar lines        

                    //mergedLine[0] = std::min(checkingline[0], mergedLine[0]);
                    mergedLine[1] = std::min(checkingline[1], mergedLine[1]);
                    //mergedLine[2] = std::max(checkingline[2], mergedLine[2]);
                    mergedLine[3] = std::max(checkingline[3], mergedLine[3]);

                    mergedLines[j] = mergedLine;
                    lineMerged = true;
                    break;
                }
            }

            if (!lineMerged)
            {
                // Add line as a new merged line
                mergedLines.push_back(checkingline);
            }
        }
        sort(mergedLines.begin(), mergedLines.end(), compareLinesByX);

        mergedLines.erase(remove_if(mergedLines.begin(), mergedLines.end(), shouldDeleteLine), mergedLines.end());

        vector<double> distances;
        for (size_t k = 1; k < mergedLines.size(); ++k) {
            double distance = abs(mergedLines[k - 1][0] - mergedLines[k][0]);
            distances.push_back(distance);

            if (k == 5) {
                distanceToCheck = distance;
            }
        }

        sort(distances.begin(), distances.end());


        double medianDistance = 0;
        double deviationThreshold = 0;

        if (distances.size() != 0) {
            medianDistance = distances[distances.size() / 2];
            deviationThreshold = medianDistance / 1.5;
        } 
        

        if (mergedLines.size() != 0){
            for (auto iter = mergedLines.begin(); iter + 1 != mergedLines.end(); ++iter) {

                double distanceCheck = abs(((iter)->val[2]) - ((iter + 1)->val[2]));

                if (distanceCheck < deviationThreshold) {
                    iter = mergedLines.erase(iter);
                    --iter;  // Adjust the iterator position after erasing an element
                }
            }
        }

        double y_avg_up = 0;
        double y_avg_down = 0;

        for (size_t i = 0; i < mergedLines.size(); i++)
        {
            Vec4i line = mergedLines[i];
            y_avg_up = y_avg_up + mergedLines[i][1];
            y_avg_down = y_avg_down + mergedLines[i][3];

            //cv::line(Fretboard, Point(line[0], line[1]), Point(line[2], line[3]), Scalar(255, 0, 255), 2);
        }

        y_avg_up = y_avg_up / mergedLines.size();
        y_avg_down = y_avg_down / mergedLines.size();

        Point left_up = Point(mergedLines.front()[0], y_avg_up);
        Point right_up = Point(mergedLines.back()[0], y_avg_up);
        Point left_down = Point(mergedLines.front()[2], y_avg_down);
        Point right_down = Point(mergedLines.back()[2], y_avg_down);

        //line(Fretboard, left_up, right_up, Scalar(255, 255, 0), 2);
        //line(Fretboard, left_down, right_down, Scalar(255, 255, 0), 2);

        //showImage("Sobel Filter - Frets Detected", Fretboard);

        // Calculate vertical gradient using Sobel operator
        Mat gradientH;
        Sobel(Fretboard2, gradientH, CV_16S, 0, 1, -1);

        // Convert gradient image to absolute values
        Mat gradientAbsH;
        convertScaleAbs(gradientH, gradientAbsH);

        // Apply thresholding
        Mat thresholdedH;
        threshold(gradientAbsH, thresholdedH, 254, 255, THRESH_BINARY);

        // Apply median filter to thresholded image
        Mat medianFilteredH;
        medianBlur(thresholdedH, thresholdedH, 3);

        // Perform morphological closing operation
        morphologyEx(thresholdedH, thresholdedH, MORPH_CLOSE, morphKernel);

        //showImage("Sobel Filter for Strings", thresholdedH);
        
        vector<Vec4i> linesFretboardH, stringsFretboard;

        HoughLinesP(thresholdedH, linesFretboardH, 1, CV_PI / 180, 20, 200, 10); //10 100 10
        for (size_t i = 0; i < linesFretboardH.size(); i++) {

            if (linesFretboardH[i][0] < linesFretboardH[i][2]) {
                linesFretboardH[i] = linesFretboardH[i];
                stringsFretboard.push_back(linesFretboardH[i]);

            }
            else {
                double temp1 = linesFretboardH[i][0];
                double temp2 = linesFretboardH[i][1];
                linesFretboardH[i][0] = linesFretboardH[i][2];
                linesFretboardH[i][1] = linesFretboardH[i][3];
                linesFretboardH[i][2] = temp1;
                linesFretboardH[i][3] = temp2;
                stringsFretboard.push_back(linesFretboardH[i]);
            }

            //line(Fretboard, Point(linesFretboardH[i][0], linesFretboardH[i][1]),
            //  Point(linesFretboardH[i][2], linesFretboardH[i][3]), Scalar(0, 255, 255), 1, 8);
        }

        // Filter lines that are outside the ROI
        vector<Vec4i> linesInROI;
        for (const auto& line : linesFretboardH) {
            // Check if line endpoints are within the ROI
            if ((line[1] >= left_up.y) && (line[1] <= left_down.y) && (line[3] >= right_up.y) && (line[3] <= right_down.y))
            {
                linesInROI.push_back(line);
                //cv::line(Fretboard, Point(line[0], line[1]), Point(line[2], line[3]), Scalar(125, 125, 255));
            }
        }

        //showImage("Sobel Filter for Strings", Fretboard);

        // Merge similar lines
        vector<Vec4i> mergedLinesStrings;
        double maxDistanceDiffH = 20; //15 // Maximum distance difference to consider lines similar
        for (size_t i = 0; i < linesInROI.size(); i++) {
            Vec4i checkingline = linesInROI[i];
            bool lineMerged = false;

            for (size_t j = 0; j < mergedLinesStrings.size(); j++)
            {
                Vec4i mergedLine = mergedLinesStrings[j];
                if (areLinesSimilarH(checkingline, mergedLine, maxDistanceDiffH))
                {
                    // Merge similar lines        

                    mergedLine[0] = std::min(checkingline[0], mergedLine[0]);
                    //mergedLine[1] = std::min(checkingline[1], mergedLine[1]);
                    mergedLine[2] = std::max(checkingline[2], mergedLine[2]);
                    //mergedLine[3] = std::max(checkingline[3], mergedLine[3]);

                    mergedLinesStrings[j] = mergedLine;
                    lineMerged = true;
                    break;
                }
            }

            if (!lineMerged)
            {
                // Add line as a new merged line
                mergedLinesStrings.push_back(checkingline);
            }
        }

        sort(mergedLinesStrings.begin(), mergedLinesStrings.end(), compareLinesByY);

        for (size_t i = 0; i < mergedLinesStrings.size(); i++)
        {
            Vec4i line = mergedLinesStrings[i];
            //cv::line(Fretboard, Point(line[0], line[1]), Point(line[2], line[3]), Scalar(0, 255, 255), 2);
        }

        //cout << mergedLinesStrings.size() << endl;

        //showImage("Sobel Filter for Strings", Fretboard);

        
        vector<vector<Point>> contours;
        vector<vector<Point>> fingerContours;
        vector<Point> fingertips;

        cv::findContours(edgesFretboard.clone(), contours, RETR_LIST, CHAIN_APPROX_SIMPLE);

        for (const auto& contour : contours) {
            double area = contourArea(contour);
            if (area > 1300) {

                Rect boundingRect = cv::boundingRect(contour);

                // Perform RGB thresholding on the contour
                Mat rgbROI = Fretboard(boundingRect);
                Scalar meanColor = cv::mean(rgbROI);

                // Adjust the RGB threshold values as needed
                Scalar lowerThreshold(20, 40, 95);
                Scalar upperThreshold(25, 255, 255);

                double minRGB = min(meanColor[0], min(meanColor[1], meanColor[2]));
                double maxRGB = max(meanColor[0], max(meanColor[1], meanColor[2]));
                double RG_diff = abs(meanColor[1] - meanColor[2]);

                if (meanColor[0] >= lowerThreshold[0] && meanColor[1] >= lowerThreshold[1] &&
                    meanColor[2] >= lowerThreshold[2] && (maxRGB - minRGB) > 15 && meanColor[2] > meanColor[1] &&
                    meanColor[2] > meanColor[0] && RG_diff > 16 && (boundingRect.x + boundingRect.width / 2 < Fretboard.cols / 2)) {

                    //fingerContours.push_back(contour);
                    bool contourValid = true;

                    Rect boundingRect = cv::boundingRect(contour);
                    int yThreshold = boundingRect.y + boundingRect.height / 2;

                    vector<Point> highestPoints;
                    vector<Point> filteredContour;
                    for (const auto& point : contour) {

                        if (mergedLines.size() == 0) {
                            continue;
                        }
                        bool check_cond11 = (point.x <= ((mergedLines.front()[0] + mergedLines.front()[2]) / 2.0));
                        bool check_cond22 = (point.x >= ((mergedLines.back()[0] + mergedLines.back()[2]) / 2.0));
                        bool check_cond33 = (point.y <= ((mergedLinesStrings.front()[1] + mergedLinesStrings.front()[3]) / 2.0));
                        bool check_cond44 = (point.y >= ((mergedLinesStrings.back()[1] + mergedLinesStrings.back()[3]) / 2.0));

                        if (check_cond11 || check_cond22 || check_cond33 || check_cond44) {

                        }
                        else {
                            filteredContour.push_back(point);
                        }
                    }

                    for (const auto& point : filteredContour) {
                        if (point.y < yThreshold) {
                            highestPoints.push_back(point);
                        }
                    }

                    if (!highestPoints.empty()) {

                        Point point_check = highestPoints.back();

                        if (mergedLines.size() != 0) {
                            bool check_cond1 = (point_check.x <= ((mergedLines.front()[0] + mergedLines.front()[2]) / 2.0));
                            bool check_cond2 = (point_check.x >= ((mergedLines.back()[0] + mergedLines.back()[2]) / 2.0));
                            bool check_cond3 = (point_check.y <= ((mergedLinesStrings.front()[1] + mergedLinesStrings.front()[3]) / 2.0));
                            bool check_cond4 = (point_check.y >= ((mergedLinesStrings.back()[1] + mergedLinesStrings.back()[3]) / 2.0));

                            if (check_cond1 || check_cond2 || check_cond3 || check_cond4) {

                            }
                            else {

                                if (contourArea(filteredContour) > 100) {
                                    fingerContours.push_back(filteredContour);
                                    fingertips.push_back(point_check); // highestPoints.back());
                                }

                            }
                        }                        
                    }
                }
            }
        }
        
        for (const auto& contour : fingerContours) {
            drawContours(Fretboard, vector<vector<Point>>{contour}, -1, Scalar(0, 255, 0), 2);
        }

        //showImage("Finger Detection", Fretboard);

        for (const auto& fingertip : fingertips) {
            circle(Fretboard, fingertip, 3, Scalar(0, 0, 255), -1);
        }

        //showImage("Finger Detection - Fingertips", Fretboard);

        for (size_t i = 0; i < mergedLinesStrings.size(); i++)
        {
            Vec4i line = mergedLinesStrings[i];
            Point intersectionPoint = calculateIntersection(line, mergedLines.front());
            extendLineToPoint(line, intersectionPoint);
            cv::line(Fretboard, Point(line[0], line[1]), Point(line[2], line[3]), Scalar(0, 255, 255), 2);
        }

        if (distanceToCheck != 0){
            fillMissingLineSegments(mergedLines, distanceToCheck, Fretboard);
        }         
        
        sort(mergedLines.begin(), mergedLines.end(), compareLinesByX);

        for (size_t i = 0; i < mergedLines.size(); i++)
        {
            Vec4i line = mergedLines[i];
            cv::line(Fretboard, Point(line[0], line[1]), Point(line[2], line[3]), Scalar(255, 0, 255), 2);
        }

        showImage("Final", Fretboard);

        

        Chord fingerpositions = { 0,0,0,0,0,0 };

        for (const auto& fingertip : fingertips) {

            int fret_num = 0;

            for (size_t f = 0; f < mergedLines.size(); f++) {
                Vec4i fret = mergedLines[f];

                if (fingertip.x > fret[0]) {
                    continue;
                }
                else {
                    fret_num = f;
                    break;
                }

            }

            for (size_t m = 0; m < 6; m++) {
                Vec4i string = mergedLinesStrings[m];

                if (fingertip.y > string[1]) {
                    continue;
                }
                else {
                    fingerpositions.at(m) = fret_num;
                    break;
                }

            }
        }

        for (int i = 0; i < fingerpositions.size(); i++) {
            cout << fingerpositions.at(i) << " ";
        }
        cout << endl;

        // Write the images
        resultOutput(frame_blur, frame_edge, frame_orig, frame_rotated, Fretboard);

        return fingerpositions;
}