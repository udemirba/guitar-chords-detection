#include "projectHeader.h" // Include the header file

using namespace std;
using namespace cv;

void getRGBFrame(Mat* img, string image_full_path) {
    /* The getRGBFrame function takes cv::Mat class pointer.It finds the
     image path, then read the values and stores them in the cv::Mat class. */   

    /*cout << "Please give the full filename of the RGB image: "; //taking input file
    cin >> img_name;
    cout << endl;

    try {
        image_full_path = samples::findFile(folder_name + img_name); //finding the full path
    }
    catch (Exception& e) {
        cerr << "No such frame file is found. Please re-run the program!" << endl;
        exit(0);
    }*/

    //reads the values of the images
    *img = imread(image_full_path);

    
}

// Helper function to compute Euclidean distance between two points
double euclideanDistance(const Point2f& pt1, const Point2f& pt2) {
    double dist_x = pt1.x - pt2.x;
    double dist_y = pt1.y - pt2.y;
    return sqrt(dist_x * dist_x + dist_y * dist_y);
}

double distanceToLine(const Point2f& point, const Point2f& lineStart, const Point2f& lineEnd) {
    double A = lineEnd.y - lineStart.y;
    double B = lineStart.x - lineEnd.x;
    double C = lineEnd.x * lineStart.y - lineStart.x * lineEnd.y;
    double distance = std::abs(A * point.x + B * point.y + C) / std::sqrt(A * A + B * B);
    return distance;
}

// Helper function to find the largest cluster of lines
vector<Vec4i> findLargestCluster(vector<Vec4i> lines, double thresholdDistance, Mat image) {
    // Get the first line as the reference line
    Vec4f referenceLine = lines[0];

    // Create a new vector to store the lines with distances smaller than the threshold
    vector<Vec4i> lines_new;
    // Calculate the middle points of each line and find the distances to the reference line
    for (size_t i = 0; i < lines.size(); ++i) {
        Vec4i& line1 = lines[i];
        Point2f lineStart(line1[0], line1[1]);
        Point2f lineEnd(line1[2], line1[3]);
        Point2f middlePoint = (lineStart + lineEnd) * 0.5;
        double distance = distanceToLine(middlePoint, Point2f(referenceLine[0], referenceLine[1]), Point2f(referenceLine[2], referenceLine[3]));
        if (distance < thresholdDistance) {
            lines_new.push_back(line1);
        }
    }
    
    return lines_new;
}


void resultOutput(Mat frame_blur, Mat frame_edge, Mat frame_orig, Mat frame_rotated, Mat frame_fretboard) {

    imwrite(outFolder + "Blurred Image.jpg", frame_blur);
    imwrite(outFolder + "Canny Detected Edges.jpg", frame_edge);
    imwrite(outFolder + "result_im.jpg", frame_orig);
    imwrite(outFolder + "rotated_im.jpg", frame_rotated);
    imwrite(outFolder + "Fretboard.jpg", frame_fretboard);
}

void showImage(string win_name, Mat image_mat) {
    namedWindow(win_name, WINDOW_NORMAL);
    imshow(win_name, image_mat);
    waitKey(0);
}

Mat extractRoI(vector<Vec4i> lines, Mat frame) {

    vector<Point2f> lineEndpoints;
    Mat croppedImage;

    for (size_t j = 0; j < lines.size(); j++) {
        lineEndpoints.push_back(Point2f(lines[j][0], lines[j][1]));
        lineEndpoints.push_back(Point2f(lines[j][2], lines[j][3]));
    }

    // Find the rotated bounding box of the lines
    RotatedRect rotatedRect = minAreaRect(lineEndpoints);

    // Get the vertices of the rotated rectangle
    Point2f vertices[4];
    rotatedRect.points(vertices);

    // Draw the ROI rectangle on the image
    for (int i = 0; i < 4; i++) {
        //line(frame, vertices[i], vertices[(i + 1) % 4], Scalar(255, 0, 0), 2);
    }

    // Convert the vertices from Point2f to Point
    vector<Point> roiVertices;
    for (const auto& vertex : vertices) {
        roiVertices.push_back(Point(vertex.x, vertex.y));
    }

    // Create a mask for the ROI region
    Mat roiMask = Mat::zeros(frame.size(), CV_8UC1);
    fillConvexPoly(roiMask, roiVertices.data(), roiVertices.size(), Scalar(255));

    frame.copyTo(croppedImage, roiMask);

    return croppedImage;
}

Mat extractRotatedRoI(vector<Vec4i> rotated_lines, Mat frame_rotated, Mat rotation_matrix) {

    vector<Point2f> rotatedLineEndpoints;

    for (size_t k = 0; k < rotated_lines.size(); k++) {
        Mat pt1 = (Mat_<double>(3, 1) << rotated_lines[k][0], rotated_lines[k][1], 1);
        Mat pt2 = (Mat_<double>(3, 1) << rotated_lines[k][2], rotated_lines[k][3], 1);

        // Rotate the line endpoints using the rotation matrix
        Mat rotatedPt1 = rotation_matrix * pt1;
        Mat rotatedPt2 = rotation_matrix * pt2;

        rotatedLineEndpoints.push_back(Point2f(rotatedPt1.at<double>(0), rotatedPt1.at<double>(1)));
        rotatedLineEndpoints.push_back(Point2f(rotatedPt2.at<double>(0), rotatedPt2.at<double>(1)));
    }

    // Draw the rotated ROI rectangle on the image
    RotatedRect rotatedRect2 = minAreaRect(rotatedLineEndpoints);
    Point2f verticesRot[4];
    rotatedRect2.points(verticesRot);

    // Convert the vertices from cv::Point2f to cv::Point
    vector<Point> roiRotatedVertices;
    for (const auto& vertex : verticesRot) {
        roiRotatedVertices.push_back(Point(vertex.x, vertex.y));
    }

    // Create a mask for the ROI region
    Mat roiRotatedMask = Mat::zeros(frame_rotated.size(), CV_8UC1);
    fillConvexPoly(roiRotatedMask, roiRotatedVertices.data(), roiRotatedVertices.size(), Scalar(255));
    Mat croppedRotatedImage;
    frame_rotated.copyTo(croppedRotatedImage, roiRotatedMask);

    // Find the bounding box of the non-zero pixels
    Rect boundingBox = boundingRect(roiRotatedMask);

    // Crop the masked image using the bounding box
    Mat croppedFinalImage = croppedRotatedImage(boundingBox);

    return croppedFinalImage;
}


bool areLinesSimilar(const Vec4i& line1, const Vec4i& line2, double maxDistanceDiff) {

    Point p1(line1[0], line1[1]);
    Point p2(line1[2], line1[3]);
    Point p3(line2[0], line2[1]);
    Point p4(line2[2], line2[3]);

    Point p_bottom1, p_bottom2, p_up1, p_up2;

    if (p1.y < p2.y) {
        p_bottom1 = p1;
        p_up1 = p2;
    }
    else if (p1.y >= p2.y) {
        p_bottom1 = p2;
        p_up1 = p1;
    }

    if (p3.y < p4.y) {
        p_bottom2 = p3;
        p_up2 = p4;
    }
    else if (p3.y >= p4.y) {
        p_bottom2 = p4;
        p_up2 = p3;
    }

    double distance_bottom = abs(p_bottom1.x - p_bottom2.x);
    double distance_up = abs(p_up1.x - p_up2.x);
    double distanceAvg = (distance_bottom + distance_up) / 2.0;


    return (distanceAvg <= maxDistanceDiff);
}

bool compareLinesByX(const cv::Vec4i& line1, const cv::Vec4i& line2) {
    return line1[0] < line2[0];  // Assuming line[0] corresponds to the x-coordinate of the starting point
}

bool compareLinesByY(const cv::Vec4i& line1, const cv::Vec4i& line2) {
    return line1[1] < line2[1];  // Assuming line[0] corresponds to the x-coordinate of the starting point
}

bool areLinesSimilarH(const Vec4i& line1, const Vec4i& line2, double maxDistanceDiff) {

    Point p1(line1[0], line1[1]);
    Point p2(line1[2], line1[3]);
    Point p3(line2[0], line2[1]);
    Point p4(line2[2], line2[3]);

    Point p_left1, p_left2, p_right1, p_right2;

    if (p1.x < p2.x) {
        p_left1 = p1;
        p_right1 = p2;
    }
    else if (p1.x >= p2.x) {
        p_left1 = p2;
        p_right1 = p1;
    }

    if (p3.x < p4.x) {
        p_left2 = p3;
        p_right2 = p4;
    }
    else if (p3.x >= p4.x) {
        p_left2 = p4;
        p_right2 = p3;
    }

    double distance_left = abs(p_left1.y - p_left2.y);
    double distance_right = abs(p_right1.y - p_right2.y);
    double distanceAvg = (distance_left + distance_right) / 2.0;

    return (distanceAvg <= maxDistanceDiff);
}

bool shouldDeleteLine(const cv::Vec4i& line) {

    double x1 = line[0];
    double y1 = line[1];
    double x2 = line[2];
    double y2 = line[3];

    double slope = (y1 - y2) / (x1 - x2);

    return (x1 < 10 || (slope < 5 && slope >= -5));  // Delete line if it starts before x=10
}


// Function to calculate the intersection point of two lines
Point calculateIntersection(Vec4i line1, Vec4i line2) {
    // Extract the start and end points of line1
    Point line1Start(line1[2], line1[3]);
    Point line1End(line1[0], line1[1]);

    // Calculate the slope and intercept of line1
    double slope1 = static_cast<double>(line1End.y - line1Start.y) / (line1End.x - line1Start.x);
    double intercept1 = line1Start.y - slope1 * line1Start.x;

    // Extract the start and end points of line2
    Point line2Start(line2[0], line2[1]);
    Point line2End(line2[2], line2[3]);

    // Calculate the slope and intercept of line2
    double slope2 = static_cast<double>(line2End.y - line2Start.y) / (line2End.x - line2Start.x);
    double intercept2 = line2Start.y - slope2 * line2Start.x;

    // Calculate the intersection point
    double intersectionX = (intercept2 - intercept1) / (slope1 - slope2);
    double intersectionY = slope1 * intersectionX + intercept1;

    return Point(static_cast<int>(intersectionX), static_cast<int>(intersectionY));
}

// Function to extend a line up to a specified point
void extendLineToPoint(Vec4i& line, Point point) {
    line[0] = point.x;
    line[1] = point.y;
}

// Function to fill missing line segments between parallel lines
void fillMissingLineSegments(vector<Vec4i>& lines, double avgDistance, Mat& image) {
    vector<Vec4i> missingLines;

    for (size_t i = 0; i < lines.size() - 1; ++i) {
        Vec4i line1 = lines[i];
        Vec4i line2 = lines[i + 1];

        // Calculate the distance between the two parallel lines
        double distance = norm(Point(line1[0], line1[1]) - Point(line2[0], line2[1]));

        // Calculate the number of missing line segments
        int numMissingSegments = static_cast<int>(distance / avgDistance) - 1;

        // Calculate the step size for drawing the missing line segments
        double stepSizeX = static_cast<double>(line2[0] - line1[0]) / (numMissingSegments + 1);
        double stepSizeY = static_cast<double>(line2[1] - line1[1]) / (numMissingSegments + 1);

        // Draw and fill the missing line segments
        for (int j = 1; j <= numMissingSegments; ++j) {
            Point startPoint(static_cast<int>(line1[0] + j * stepSizeX), static_cast<int>(line1[1] + j * stepSizeY));
            Point endPoint(static_cast<int>(line1[2] + j * stepSizeX), static_cast<int>(line1[3] + j * stepSizeY));

            missingLines.push_back(Vec4i(startPoint.x, startPoint.y, endPoint.x, endPoint.y));
        }
    }

    // Append the missing lines to the lines vector
    lines.insert(lines.end(), missingLines.begin(), missingLines.end());
}

// Function to display the chord library as basic charts
void displayChordLibrary(const vector<Chord>& chordLibrary, const vector<string>& chordLibraryNames) {
    // Iterate over the chord library
    cout << "Chord Library:" << endl;
    for (size_t i = 0; i < chordLibrary.size(); ++i) {
        cout << "Chord Name: " << chordLibraryNames[i] << endl;
        cout << "-------" << endl;
        for (int fret : chordLibrary[i]) {
            cout << "|" << fret << "|";
        }
        cout << endl;
        cout << "-------" << endl;
    }
}

int isChordInLibrary(const Chord& chord) {
    // Iterate over the chord library
    for (size_t i = 0; i < chordLibrary.size(); ++i) {
        // Check if the chord matches any chord in the library
        if (chord == chordLibrary[i]) {
            return static_cast<int>(i);
        }
    }
    return -1; // Chord not found in the library
}

// Function to find the most frequently occurring chord from the stored chords
int findMostFrequentChord(const std::vector<int>& storedChords) {
    std::unordered_map<int, int> frequencyMap;
    for (int chord : storedChords) {
        frequencyMap[chord]++;
    }

    int mostFrequentChord = 0;
    int maxFrequency = 0;
    for (const auto& entry : frequencyMap) {
        if (entry.second > maxFrequency) {
            mostFrequentChord = entry.first;
            maxFrequency = entry.second;
        }
    }

    return mostFrequentChord;
}