// Bogazici University - Electrical & Electronics Engineering Department
// Term : 2022/2023-2 
// Course : EE 475 - Introduction to Image Processing
// Instructor : B�lent Sankur
//
// Term Project 
// Authors : Umut Demirbas - Arda Ozgun 
// Date : 07.06.2023


#ifndef PROJECTHEADER_H // #include guards to avoid multiple definitions
#define PROJECTHEADER_H

#include <opencv2/core.hpp> //including some useful libraries for the project
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
//#include <opencv2/calib3d.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
//#include <vector>
//#include <queue>
//#include <cstdlib>
//#include <math.h>
//#include <sys/stat.h>
#include <unordered_map>
#include <algorithm>
#include <cmath>


using namespace std;
using namespace cv;
typedef vector<int> Chord;

//function declarations

void getRGBFrame(Mat* img, string image_full_path);
void resultOutput(Mat frame_blur, Mat frame_edge, Mat frame_orig, Mat frame_rotated, Mat frame_fretboard);
void showImage(string win_name, Mat image_mat);
Mat extractRoI(vector<Vec4i> lines, Mat frame);
Mat extractRotatedRoI(vector<Vec4i> rotated_lines, Mat frame_rotated, Mat rotation_matrix);
bool areLinesSimilar(const Vec4i& line1, const Vec4i& line2, double maxDistanceDiff);
bool areLinesSimilarH(const Vec4i& line1, const Vec4i& line2, double maxDistanceDiff);
bool compareLinesByX(const cv::Vec4i& line1, const cv::Vec4i& line2);
bool compareLinesByY(const cv::Vec4i& line1, const cv::Vec4i& line2);
bool shouldDeleteLine(const cv::Vec4i& line);
Point calculateIntersection(cv::Vec4i line1, cv::Vec4i line2);
void extendLineToPoint(cv::Vec4i& line, cv::Point point);
void fillMissingLineSegments(vector<Vec4i>& lines, double avgDistance, Mat& image);

vector<Vec4i> findLargestCluster(vector<Vec4i> lines, double thresholdDistance, Mat image);
double distanceToLine(const Point2f& point, const Point2f& lineStart, const Point2f& lineEnd);
Chord processFrame(Mat& frame);

void displayChordLibrary(const vector<Chord>& chordLibrary, const vector<string>& chordLibraryNames);
int isChordInLibrary(const Chord& chord);
int findMostFrequentChord(const std::vector<int>& storedChords);

namespace { //parameter declarations 
	string img_name;
	string folder_name = "Data/";
	string outFolder = "Result/";
	string chartFolder = "Charts/";

	const int BUFFER_SIZE = 2;

    const vector<Chord> chordLibrary = {
    { 0, 0, 2, 2, 2, 0 }, //A
    { 0, 0, 2, 2, 1, 0 }, //Am
    { 0, 2, 1, 2, 0, 2 }, //B7
    { 0, 3, 2, 0, 1, 0 }, //C
    { 0, 0, 0, 2, 3, 2 }, //D
    { 0, 0, 0, 2, 3, 1 }, //Dm
    { 0, 2, 0, 1, 0, 0 }, //E7
    { 0, 2, 2, 1, 0, 0 }, //E
    { 0, 2, 2, 0, 0, 0 }, //Em
    { 3, 2, 0, 0, 3, 3 }, //G2
    { 3, 2, 0, 0, 0, 3 }, //G

    };

    const vector<string> chordLibraryNames = {
        "A",
        "Am",
        "B7",
        "C",
        "D",
        "Dm",
        "E7",
        "E",
        "Em",
        "G2",
        "G",

    };

	double angleThreshold = 1.0;
	double distanceThreshold = 1.0;
    int MaxStoredChords = 15;
}
#endif