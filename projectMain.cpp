#include "projectHeader.h"



int main() {    

    // Display the chord library
    displayChordLibrary(chordLibrary, chordLibraryNames);

    Chord detectedChord;
    int chord_ind = -1;

    // Ask the user for the input file path
    cout << "Please give the full filename of the RGB image or video:  ";
    string fileName;
    cin >> fileName;

    string image_full_path = samples::findFile(folder_name + fileName); //finding the full path

    // Load the input file using imread
    Mat input = imread(image_full_path);
    Mat overlayImage;

    // Check if the input file is an image or not
    if (!input.empty()) {
        // The input file is an image
        cout << "Input file is an image." << endl;

        // Process the image here
        detectedChord = processFrame(input);
        chord_ind = isChordInLibrary(detectedChord);
    
        if (chord_ind != -1) {
            overlayImage = imread(chartFolder + to_string(chord_ind) + ".png");
            Size newSize(overlayImage.cols * 2, overlayImage.rows * 2);
            resize(overlayImage, overlayImage, newSize);
            Rect roi(input.cols - overlayImage.cols, 0, overlayImage.cols, overlayImage.rows);
            overlayImage.copyTo(input(roi));
            showImage("Final Result", input);

        }
        else {
            cout << "The chord couldn't be found. Please try again." << endl;
        }

    }
    else {
        
        // The input file is not an image, treat it as a video
        cout << "Input file is a video." << endl;

        // Open the input file using VideoCapture
        VideoCapture cap(image_full_path);

        // Check if the video file is opened successfully
        if (!cap.isOpened()) {
            cout << "Failed to open the video file." << endl;
            return -1;
        }

        // Get the video's frame size and FPS
        Size frameSize(static_cast<int>(cap.get(CAP_PROP_FRAME_WIDTH)),
            static_cast<int>(cap.get(CAP_PROP_FRAME_HEIGHT)));
        double fps = cap.get(CAP_PROP_FPS);

        // Create the VideoWriter object to save the processed frames to a file
        VideoWriter outputVideo("Result/output_video.mp4", VideoWriter::fourcc('X', '2', '6', '4'), fps, frameSize);

        // Process the video frames
        Mat frame;
        vector<int> storedChords;

        int frame_num = 0;
        int mostFrequentChord = -1;
        while (cap.read(frame)) {
            frame_num += 1;
            // Process each frame of the video here
            detectedChord = processFrame(frame);
            chord_ind = isChordInLibrary(detectedChord);

            if (chord_ind != -1) {
                storedChords.push_back(chord_ind);
            }            

            if (frame_num % MaxStoredChords == 0) {
                mostFrequentChord = findMostFrequentChord(storedChords);

                if (mostFrequentChord == 0){
                    mostFrequentChord = -1;
                }

                cout << "Chords " << frame_num - MaxStoredChords + 1 << " - " << frame_num << ": Most Frequent Chord = " << mostFrequentChord << endl;

                storedChords.clear();
            }

            if (mostFrequentChord != -1) {
                overlayImage = imread(chartFolder + to_string(mostFrequentChord) + ".png");
                //showImage("a", overlayImage);
                //Size newSize(overlayImage.cols * 2, overlayImage.rows * 2);
                //resize(overlayImage, overlayImage, newSize);
                Rect roi(0, 0, overlayImage.cols, overlayImage.rows);
                overlayImage.copyTo(frame(roi));
                outputVideo.write(frame);
                //imshow("Final Result", frame);
            }
            else {
                outputVideo.write(frame);
            }
            // Display the frame
            //showImage("Video", frame);
            //waitKey(0);

            // Check for user input to exit
           // if (waitKey(1) == 27) {
              //  break;
           // }
        }

        cap.release();
        outputVideo.release();
    }
    return 0;
} 
